﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OTUS.MultiThreading
{
	class Worker
	{
		private List<int> _data;

		public int ArraySize = 10_000_000_0;// 100_000; //1_000_000, 10_000_000

		public Worker()
		{
			_data = Enumerable.Repeat(1, ArraySize).ToList();
		}
		public void RegularSum()
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var result = _data.Sum();

			stopwatch.Stop();
			Console.WriteLine($"Обычное: {stopwatch.ElapsedMilliseconds} мс.");
		}


		public void AsThread()
		{
			int sum = 0;
			var threads = new List<Thread>();

			var splitArray = Split<int>(_data, Environment.ProcessorCount);

			foreach (var item in splitArray)
			{
				threads.Add(new Thread(() =>
				{
					var subsum = item.Sum();
					Interlocked.Add(ref sum, subsum);
				}));
			}

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			threads.ForEach(x => x.Start());
			threads.ForEach(x => x.Join());

			stopwatch.Stop();
			Console.WriteLine($"Thread: {stopwatch.ElapsedMilliseconds} мс.");
		}

		public void AsParallel()
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var result = _data.AsParallel().Sum();
			stopwatch.Stop();
			Console.WriteLine($"LINQ: {stopwatch.ElapsedMilliseconds} мс.");

			return;

		}
		public static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> list, int parts)
		{
			int i = 0;
			var splits = from item in list
						 group item by i++ % parts into part
						 select part.AsEnumerable();
			return splits;
		}

	}
}

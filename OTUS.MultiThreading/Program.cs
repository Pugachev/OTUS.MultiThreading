﻿using System;

namespace OTUS.MultiThreading
{
	class Program
	{
		static async System.Threading.Tasks.Task Main(string[] args)
		{
			Console.WriteLine("Hello World!");

			Worker worker = new Worker();
			worker.RegularSum();
			worker.AsThread();
			worker.AsParallel();

			Console.ReadLine();
		}
	}
}
